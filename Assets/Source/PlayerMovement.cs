﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody rigidBody;
    public float forwardForce = 200000.0f;
    public float turnRate = 10.0f;

    float steer_max = 20;
    float motor_max = 10;
    float brake_max = 100;

    public WheelCollider rearWheel1;
    public WheelCollider rearWheel2;
    public WheelCollider frontWheel1;
    public WheelCollider frontWheel2;

    // Start is called before the first frame update
    void Start()
    {
    
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float fwd = Input.GetAxis("Vertical");
        rigidBody.AddForce(transform.forward * fwd * forwardForce * Time.deltaTime);
        //rearWheel1.motorTorque += fwd * forwardForce;
        //frontWheel1.motorTorque += fwd * forwardForce;
        float turn = Input.GetAxis("Horizontal");
        transform.Rotate(0.0f, turnRate * turn, 0.0f);
    }
}
