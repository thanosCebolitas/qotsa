﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class KochLine : KochGenerator
{
    LineRenderer lineRenderer;
    Vector3[] lerpPosition;
    public float generateMultiplier;
    private float[] lerpAudio;

    [Header("Audio")]
    public int[] audioBand;
    public Material material;
    public Color color;
    private Material matInstance;
    public int audioBandMaterial;
    public float emissionMultiplier;

    // Start is called before the first frame update
    void Start()
    {
        lerpAudio = new float[initiatorPointAmount];
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.enabled = true;
        lineRenderer.useWorldSpace = false;
        lineRenderer.loop = true;
        lineRenderer.positionCount = position.Length;
        lineRenderer.SetPositions(position);
        lerpPosition = new Vector3[position.Length];

        // apply material
        matInstance = new Material(material);
        lineRenderer.material = matInstance;
    }

    // Update is called once per frame
    void Update()
    {
        matInstance.SetColor("_Color", color * 8 * MeshGenerator.freqBandsFiltered[audioBandMaterial] * emissionMultiplier);

        if (generationCount != 0)
        {
            int count = 0;
            int bandSize = MeshGenerator.freqBandsFiltered.Length;
            for (int i = 0; i < initiatorPointAmount; i++)
            {
                lerpAudio[i] = 8 * MeshGenerator.freqBandsFiltered[audioBand[i] % bandSize];
                for (int j = 0; j < (position.Length - 1) / initiatorPointAmount; j++)
                {
                    lerpPosition[count] = Vector3.Lerp(position[count], targetPosition[count], lerpAudio[i]);
                    count++;
                }
            }
            lerpPosition[count] = Vector3.Lerp(position[count], targetPosition[count], lerpAudio[initiatorPointAmount - 1]);

            if (useBezierCurves)
            {
                bezierPosition = BezierCurve(lerpPosition, bezierVertexCount);
                lineRenderer.positionCount = bezierPosition.Length;
                lineRenderer.SetPositions(bezierPosition);
            }
            else
            {
                lineRenderer.positionCount = lerpPosition.Length;
                lineRenderer.SetPositions(lerpPosition);
            }
        }
    }
}
