﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshCollider))]
public class TerrainGenerator : MonoBehaviour
{
    // Mesh data
    Mesh mesh;

    Vector3[] vertices;
    int[] triangles;

    public int xSize = 512;
    public int zSize = 512;
    public float scale = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        mesh = new Mesh();
        mesh.MarkDynamic();
        GetComponent<MeshFilter>().mesh = mesh;
        GetComponent<MeshCollider>().sharedMesh = mesh;

        CreateShape();
        UpdateMesh();
    }

    void CreateShape()
    {
        vertices = new Vector3[(xSize + 1) * (zSize + 1)];

        for (int i = 0, z = 0; z <= zSize; z++)
        {
            for (int x = 0; x <= xSize; x++)
            {
                float y = Mathf.Abs(xSize/2 - x) > 4 ? Mathf.PerlinNoise(x * 0.3f, z * 0.3f) * 2 : 0;
                vertices[i] = new Vector3(x * scale, y, z * scale);
                i++;
            }
        }

        triangles = new int[xSize * zSize * 6];

        int vert = 0;
        int tris = 0;

        for (int z = 0; z < zSize; z++)
        {
            for (int x = 0; x < xSize; x++)
            {
                triangles[tris + 0] = vert + 0;
                triangles[tris + 1] = vert + xSize + 1;
                triangles[tris + 2] = vert + 1;
                triangles[tris + 3] = vert + 1;
                triangles[tris + 4] = vert + xSize + 1;
                triangles[tris + 5] = vert + xSize + 2;

                vert++;
                tris += 6;
            }
            vert++;
        }
    }

    // Update is called once per frame
    void UpdateMesh()
    {
        mesh.Clear();

        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();
        transform.position = new Vector3(-xSize / 2, -.0f, -zSize / 2);
    }

    void Update()
    {
        Transform pt = GameObject.Find("bike").GetComponent<Transform>();
        GetComponent<MeshRenderer>().material.SetColor("_PlayerWorldPosition", new Color(pt.position.x, pt.position.y, pt.position.z, 0.0f));
    }
}
