﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(AudioSource))]
public class MeshGenerator : MonoBehaviour
{
    // Player data
    public Transform playerTransform;

    // Mesh data
    Mesh mesh;
    public Material material;

    Vector3[] vertices;
    int[] triangles;

    public int resolution = 512;
    public int radius = 50;
    public int repeat = 3;

    // Audio data
    AudioSource audioSource;
    const int num_samples = 512;
    const int num_bands = 8;
    public static float[] samples = new float[num_samples];
    public static float[] freqBands = new float[num_bands];
    public static float[] freqBandsFiltered = new float[num_bands];

    const float scale = 50.0f;

    // Start is called before the first frame update
    void Start()
    {
        mesh = new Mesh();
        mesh.MarkDynamic();
        GetComponent<MeshFilter>().mesh = mesh;

        audioSource = GetComponent<AudioSource>();

        CreateShape();
        UpdateMesh();
    }

    void CreateShape ()
    {
        vertices = new Vector3[(resolution + 1) * (radius + 1)];

        for (int i = 0, r = 1; r <= radius; r++)
        {
            float angleStep = 2 * Mathf.PI / resolution;
            for (float theta = 2 * Mathf.PI; theta >= 0; theta -= angleStep)
            {
                float x = r * Mathf.Cos(theta);
                float z = r * Mathf.Sin(theta);
                float v = 1.0f - Mathf.Abs((r - (radius / 2.0f)) / (radius / 2.0f));
                float exactPos = (theta / (2 * Mathf.PI) + 0.5f) * repeat * (num_bands - 1);
                int samplePos = Mathf.RoundToInt(exactPos);
                float dist = 1 - Mathf.Abs(samplePos - exactPos);
                float s = freqBandsFiltered[samplePos % num_bands];
                vertices[i] = new Vector3(x, v * v * v * scale * Mathf.Log10(1+s) * dist * dist * dist, z);
                i++;
            }
        }

        triangles = new int[resolution * radius * 6];

        int vert = 0;
        int tris = 0;

        for (int z = 0; z < radius; z++)
        {
            for (int x = 0; x < resolution-1; x++)
            {
                triangles[tris + 0] = vert + 0;
                triangles[tris + 1] = vert + resolution + 1;
                triangles[tris + 2] = vert + 1;
                triangles[tris + 3] = vert + 1;
                triangles[tris + 4] = vert + resolution + 1;
                triangles[tris + 5] = vert + resolution + 2;

                vert++;
                tris += 6;
            }
            triangles[tris + 0] = vert + 0;
            triangles[tris + 1] = vert + resolution + 1;
            triangles[tris + 2] = vert - resolution + 1;
            triangles[tris + 3] = vert - resolution + 1;
            triangles[tris + 4] = vert + resolution + 1;
            triangles[tris + 5] = vert + resolution + 2;

            vert++;
            tris += 6;
            vert++;
        }
    }

    // Update is called once per frame
    void UpdateMesh()
    {
        mesh.Clear();

        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();
    }

    void Update()
    {
        GetSpectrumAudioSource();
        CreateShape();
        UpdateMesh();
        transform.position = new Vector3(playerTransform.position.x, 2.0f, playerTransform.position.z);
    }

    void GetSpectrumAudioSource()
    {
        audioSource.GetSpectrumData(samples, 0, FFTWindow.Hamming);
        int samplePosition = 0;
        for (int i = 0; i < num_bands; i++)
        {
            int numBandSamples = (int)Mathf.Ceil(Mathf.Pow(2, i+1));
            int sampleEndPosition = samplePosition + numBandSamples;
            float max = 0;
            for (; samplePosition < sampleEndPosition; samplePosition++)
            {
                max = max > samples[samplePosition] ? max : samples[samplePosition];
            }
            freqBands[i] = max;

            freqBandsFiltered[i] = 0.9f * freqBandsFiltered[i] + 0.1f * max;
        }
    }
}
