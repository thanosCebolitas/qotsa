﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Vox"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
		_NoiseTex("Noise (RGB)", 2D) = "white" {}
		_VoxelSize ("VoxelSize", Range(0, 0.3)) = 0.1
		_AlignmentFactor ("AlignmentFactor", Range(10.0, 300.0)) = 15
		_Amount ("Amount", Range(0, 10)) = .3
		_SpecularPower ("Specular Power", float) = 1
		_Gloss ("Gloss", float) = 1
		_WaveRate("WaveRate", float) = 0.2
    }
    SubShader
    {
        Tags {"Queue"="Transparent" "RenderType"="Transparent" "Glowable" = "True" }
        LOD 200

		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		Cull off

		Pass
		{
			CGPROGRAM
			#pragma require geometry
			#pragma vertex vert
			#pragma fragment frag
			#pragma geometry geom
			// Physically based Standard lighting model, and enable shadows on all light types
			//#pragma surface surf Standard fullforwardshadows vertex:vert
			#include "UnityCG.cginc"
			#include "Lighting.cginc"

			// Use shader model 3.0 target, to get nicer looking lighting
			#pragma target 3.0

			struct v2g {
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			struct appdata
			{
				float4 vertex : POSITION0;
				float4 center : POSITION1;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
			};

			fixed4 _Color;
			fixed _Amount;
			sampler2D _MainTex;
			sampler2D _NoiseTex;
			float4 _MainTex_ST;
			float _VoxelSize;
			float _AlignmentFactor;
			float _SpecularPower;
			float _Gloss;
			float _WaveRate;

			static const int INDICES[14] =
			{
				4, 
				3, 
				7, // A front
				8, // B front
				5, // C bottom
				3, // D right
				1, // E right
				4, // F top
				2, // G top
				7, // H left
				6, // I left
				5, // J bottom
				2, // K back
				1, // L back
			};

			static const float3 CUBE_NORMALS[14] =
			{
				float3(1.0, 1.0, 1.0),
				float3(-1.0, 1.0, 1.0),
				float3(1.0, -1.0, 1.0),  // A front
				float3(-1.0, -1.0, 1.0),  // B front
				float3(-1.0, -1.0, -1.0), // C bottom
				float3(-1.0, 1.0, 1.0), // D right
				float3(-1.0, 1.0, -1.0), // E right
				float3(1.0, 1.0, 1.0),  // F top
				float3(1.0, 1.0, -1.0),  // G top
				float3(1.0, -1.0, 1.0),  // H left
				float3(1.0, -1.0, -1.0),  // I left
				float3(-1.0, -1.0, -1.0), // J bottom
				float3(1.0, 1.0, -1.0), // K back
				float3(-1.0, 1.0, -1.0), // L back
			};

			[maxvertexcount(14)]
			void geom(triangle v2g IN[3], inout TriangleStream<appdata> triStream) {
				fixed3 a = IN[1].vertex - IN[0].vertex;
				fixed3 b = IN[2].vertex - IN[0].vertex;
				fixed3 n = normalize(cross(a, b));

				float4 center = floor((IN[0].vertex + IN[1].vertex + IN[2].vertex) / 3.0 * _AlignmentFactor) / _AlignmentFactor +_Amount * fixed4(n, 0.0);

				float4 v[8];
				float4 X = float4(1.0, 0.0, 0.0, 0.0) * _VoxelSize;
				float4 Y = float4(0.0, 1.0, 0.0, 0.0) * _VoxelSize;
				float4 Z = float4(0.0, 0.0, 1.0, 0.0) * _VoxelSize;
				float4 t1 = center - X - Z;
				float4 t2 = center + X - Z;
				float4 t3 = center - X + Z;
				float4 t4 = center + X + Z;
				v[0] = t1 + Y;
				v[1] = t2 + Y;
				v[2] = t3 + Y;
				v[3] = t4 + Y;
				v[4] = t1 - Y;
				v[5] = t2 - Y;
				v[6] = t4 - Y;
				v[7] = t3 - Y;


				appdata o;
				for (int i = 0; i < 14; i++) {
					o.vertex = UnityObjectToClipPos(v[INDICES[i]-1]);
					o.center = center;
					o.uv = IN[0].uv;
					o.normal = mul(unity_ObjectToWorld, CUBE_NORMALS[i]);
					triStream.Append(o);
				}
			}

			appdata vert(appdata v)
			{
				appdata o;
				o.vertex = v.vertex;
				o.center = v.center;
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.normal = v.normal;
				UNITY_TRANSFER_FOG(o, o.vertex);
				return o;
			}

			float4 simpleLambert(fixed3 normal, fixed3 viewDirection, fixed4 col) {
				fixed3 lightDir = _WorldSpaceLightPos0.xyz; // Light direction
				fixed3 lightCol = _LightColor0.rgb; // Light color

				fixed NdotL = max(dot(normal, lightDir), 0.2);
				float4 c;
				// FIXME: missing light buffer, after changing to deferred lightning
				//c.rgb = _Color * lightCol * NdotL;
				c = col * NdotL;

				// Specular
				fixed3 h = (lightDir);
				fixed s = max(pow(dot(normal, h), _SpecularPower) * _Gloss, 0.2);
				c.rgb = lerp(c.rgb, c.rgb + s, 0.5);
				c.a = 1;
				//c = clamp(c, 0, 1);
				return c;
			}

			float3 rgb2hsv(float3 c)
			{
				float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
				float4 p = lerp(float4(c.bg, K.wz), float4(c.gb, K.xy), step(c.b, c.g));
				float4 q = lerp(float4(p.xyw, c.r), float4(c.r, p.yzx), step(p.x, c.r));

				float d = q.x - min(q.w, q.y);
				float e = 1.0e-10;
				return float3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
			}

			float3 hsv2rgb(float3 c)
			{
				float4 K = float4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
				float3 p = abs(frac(c.xxx + K.xyz) * 6.0 - K.www);
				return c.z * lerp(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
			}

			fixed4 frag(appdata i) : SV_Target
			{
				float4 col = tex2D(_MainTex, i.uv) * _Color;
				float4 noise = tex2D(_NoiseTex, _Time / 4.0 + i.center.rb * _WaveRate);
				float3 hsv = rgb2hsv(col.rgb);
				//hsv.r = frac(hsv.r * sin(_Time.y));
				col.rgb = hsv2rgb(hsv);
				float3 viewDirection = normalize(i.vertex - _WorldSpaceCameraPos);
				col = simpleLambert(i.normal, viewDirection, col);
				col.a = pow(noise.r, 2.0);
				//return float4(i.vertex2.g, 0.0, 0.0, 1.0);
				return col;
			}
			ENDCG
		}
    }
}
