﻿Shader "Unlit/RayMarching"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Color("Color", color) = (1, 1, 0, 1)
		_SpecularPower("Specular Power", float) = 1
		_Gloss("Gloss", float) = 1
		_Angle("Angle", float) = 0
		_CX("CX", float) = 2
		_CY("CY", float) = 1
		_CZ("CZ", float) = 1
		_Scale("Scale", float) = 5
    }
    SubShader
    {
        Tags {"Queue"="Transparent" "RenderType"="Transparent" }
        LOD 100

		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite off

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
			#include "Lighting.cginc"

			#define STEPS 128
			#define MIN_DISTANCE 0.01
			#define MI 10
			#define bailout 1000

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
				float3 wPos : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

			float4 _Color;
			float _SpecularPower;
			float _Gloss;
			float _Angle;
			float _CX;
			float _CY;
			float _CZ;
			float _Scale;

			float3 RotateAroundXInDegrees(float3 vertex, float degrees)
			{
				float alpha = degrees * UNITY_PI / 180.0;
				float sina, cosa;
				sincos(alpha, sina, cosa);
				float2x2 m = float2x2(cosa, -sina, sina, cosa);
				return float3(mul(m, vertex.yz), vertex.x).yzx;
			}

			float3 RotateAroundZInDegrees(float3 vertex, float degrees)
			{
				float alpha = degrees * UNITY_PI / 180.0;
				float sina, cosa;
				sincos(alpha, sina, cosa);
				float2x2 m = float2x2(cosa, -sina, sina, cosa);
				return float3(mul(m, vertex.xy), vertex.z).xyz;
			}

			float3 RotateAroundYInDegrees(float3 vertex, float degrees)
			{
				float alpha = degrees * UNITY_PI / 180.0;
				float sina, cosa;
				sincos(alpha, sina, cosa);
				float2x2 m = float2x2(cosa, -sina, sina, cosa);
				return float3(vertex.y, mul(m, vertex.xz)).yxz;
			}

			float4x4 rotationMatrix(float3 axis, float angle)
			{
				axis = normalize(axis);
				float s = sin(angle);
				float c = cos(angle);
				float oc = 1.0 - c;

				return float4x4(oc * axis.x * axis.x + c, oc * axis.x * axis.y - axis.z * s, oc * axis.z * axis.x + axis.y * s, 0.0,
					oc * axis.x * axis.y + axis.z * s, oc * axis.y * axis.y + c, oc * axis.y * axis.z - axis.x * s, 0.0,
					oc * axis.z * axis.x - axis.y * s, oc * axis.y * axis.z + axis.x * s, oc * axis.z * axis.z + c, 0.0,
					0.0, 0.0, 0.0, 1.0);
			}

			float4 simpleLambert(fixed3 normal, fixed3 viewDirection) {
				// we are actually going to ignore the scene light and "realism"
				// because this way the model looks way cooler
				//fixed3 lightDir = _WorldSpaceLightPos0.xyz; // Light direction
				// TODO: maybe try "emitting light" from the object itself, might look cool too

				// create a static directional light "in front of the model"
				fixed3 lightDir = normalize(float4(0.0, 1.0, 1.0, 1.0));
				//fixed3 lightCol = _LightColor0.rgb; // Light color

				fixed NdotL = max(dot(normal, lightDir), 0);
				float4 c;
				// FIXME: missing light buffer, after changing to deferred lightning
				//c.rgb = _Color * lightCol * NdotL;
				c.rgb = _Color * NdotL;

				// Specular
				fixed3 h = (lightDir - viewDirection) / 2.;
				fixed s = pow(dot(normal, h), _SpecularPower) * _Gloss;
				c.rgb = lerp(c.rgb, c.rgb + s, 0.5);
				c.a = 1;
				c = clamp(c, 0, 1);
				return c;
			}

			float Menger3(float x, float y, float z) {
				float r = x * x + y * y + z * z;
				for (int i = 0; i<MI && r<bailout; i++) {
					float3 xyz = RotateAroundYInDegrees(float3(x, y, z), _Angle);

					x = abs(xyz.x); y = abs(xyz.y); z = abs(xyz.z);
					if (x - y<0) { float x1 = y; y = x; x = x1; }
					if (x - z<0) { float x1 = z; z = x; x = x1; }
					if (y - z<0) { float y1 = z; z = y; y = y1; }

					//xyz = RotateAroundXInDegrees(float3(x, y, z), _Angle / 2.0);
					//x = abs(xyz.x); y = abs(xyz.y); z = abs(xyz.z);

					x = _Scale * x - _CX * (_Scale - 1);
					y = _Scale * y - _CY * (_Scale - 1);
					z = _Scale * z;
					if (z>0.5*_CZ*(_Scale - 1)) z -= _CZ * (_Scale - 1);

					r = x * x + y * y + z * z;
				}
				return (sqrt(x*x + y * y + z * z) - 4) * pow(_Scale, -i);
			}

			float map(float3 p)
			{
				return Menger3(p.x, p.y, p.z);
			}

			float3 normal(float3 p)
			{
				const float eps = 0.01;

				return normalize
				(float3
					(map(p + float3(eps, 0, 0)) - map(p - float3(eps, 0, 0)),
						map(p + float3(0, eps, 0)) - map(p - float3(0, eps, 0)),
						map(p + float3(0, 0, eps)) - map(p - float3(0, 0, eps))
						)
				);
			}

            v2f vert (appdata v)
            {
                v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
                o.wPos = mul(unity_ObjectToWorld, v.vertex) / 2 - float3(0.0, 4.0, 0.0);
				//o.wPos = RotateAroundZInDegrees(o.wPos, _Time * 300);
				//o.wPos = RotateAroundXInDegrees(o.wPos, _Time * 300);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

			float4 renderSurface(float3 p, float3 direction)
			{
				float3 n = normal(p);
				return simpleLambert(n, direction);
			}

			float4 raymarch(float3 position, float3 direction)
			{
				for (int i = 0; i < STEPS; i++)
				{
					float distance = Menger3(position.x, position.y, position.z);
					if (distance < MIN_DISTANCE)
						return renderSurface(position, direction) * 1.1;

					position += distance * direction;
				}
				return float4(0, 0, 0, 0);
			}

            float4 frag (v2f i) : SV_Target
            {
                // sample the texture
                //float4 col = tex2D(_MainTex, i.uv);
                // apply fog
                //UNITY_APPLY_FOG(i.fogCoord, col);
				float3 worldPosition = i.wPos;
				//float3 viewDirection = normalize(i.wPos - RotateAroundYInDegrees(_WorldSpaceCameraPos, _Time * 300));
				float3 viewDirection = normalize(i.wPos - _WorldSpaceCameraPos);
				return raymarch(worldPosition, viewDirection);
            }
            ENDCG
        }
    }
}
