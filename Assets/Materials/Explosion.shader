﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Vox"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
		_VoxelSize ("VoxelSize", Range(0, 10)) = 0.3
		_Amount("Amount", Range(0, 10)) = .3
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "Glowable" = "True" }
        LOD 200

		Pass
		{
			Cull off
			CGPROGRAM
			#pragma require geometry
			#pragma vertex vert
			#pragma fragment frag
			#pragma geometry geom
			// Physically based Standard lighting model, and enable shadows on all light types
			//#pragma surface surf Standard fullforwardshadows vertex:vert
			#include "UnityCG.cginc"

			// Use shader model 3.0 target, to get nicer looking lighting
			#pragma target 3.0

			struct v2g {
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : POSITION;
			};

			fixed4 _Color;
			fixed _Amount;
			sampler2D _MainTex;
			float4 _MainTex_ST;

			[maxvertexcount(3)]
			void geom(triangle v2g IN[3], inout TriangleStream<appdata> triStream) {
				fixed3 a = IN[1].vertex - IN[0].vertex;
				fixed3 b = IN[2].vertex - IN[0].vertex;
				fixed3 n = normalize(cross(a, b));

				appdata o;
				o.vertex = UnityObjectToClipPos(IN[0].vertex + _Amount * fixed4(n, 0.0));
				o.uv = IN[0].uv;
				o.normal = n;
				triStream.Append(o);
				o.vertex = UnityObjectToClipPos(IN[1].vertex + _Amount * fixed4(n, 0.0));
				o.uv = IN[1].uv;
				o.normal = n;
				triStream.Append(o);
				o.vertex = UnityObjectToClipPos(IN[2].vertex + _Amount * fixed4(n, 0.0));
				o.uv = IN[2].uv;
				o.normal = n;
				triStream.Append(o);
			}

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = v.vertex;
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				//UNITY_TRANSFER_FOG(o, o.vertex);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				float4 col = tex2D(_MainTex, i.uv) * _Color;
				return col;
			}
			ENDCG
		}
    }
}
