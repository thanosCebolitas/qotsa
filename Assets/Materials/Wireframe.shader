﻿

Shader "Custom/Geometry/Wireframe"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_WireframeVal("Wireframe width", Range(0.000, 10)) = 1
		_Color("Color", color) = (1, 1, 1, 1)
		_FaceColor("FaceColor", color) = (0, 0, 0, 1)
		_Intensity("Intensity", Range(0.000, 10)) = 1.0
		_Amount("Amount", Range(0.000, 100)) = 0.0
		_PlayerWorldPosition("PlayerWorldPosition", color) = (0, 0, 0, 0)
		_ExplosionRange("ExplosionRange", Range(0.000, 100)) = 10.0
		_DistFade("DistFade", Range(0.000, 10)) = 6.0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "Glowable" = "True" }
		

		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha
			Cull Back

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma geometry geom
			#include "UnityCG.cginc"

			struct v2g {
				float4 pos : SV_POSITION;
			};

			struct g2f {
				float4 pos : POSITION;
				float3 bary : TEXCOORD0;
				float dist : TEXCOORD1;
			};

			struct g2v {
				float4 pos : POSITION;
				float3 bary : TEXCOORD0;
			};

			float _Amount;
			float _DistFade;
			fixed4 _PlayerWorldPosition;
			float _ExplosionRange;

			[maxvertexcount(3)]
			void geom(triangle v2g IN[3], inout TriangleStream<g2f> triStream) {
				fixed3 a = IN[1].pos - IN[0].pos;
				fixed3 b = IN[2].pos - IN[0].pos;
				fixed3 n = normalize(cross(a, b));

				float r1 = length(mul(unity_ObjectToWorld, IN[0].pos) - _PlayerWorldPosition) / _ExplosionRange;
				float r2 = length(mul(unity_ObjectToWorld, IN[1].pos) - _PlayerWorldPosition) / _ExplosionRange;
				float r3 = length(mul(unity_ObjectToWorld, IN[2].pos) - _PlayerWorldPosition) / _ExplosionRange;

				float4 tpos1 = UnityObjectToClipPos(IN[0].pos + _Amount * pow(r1, 4) * fixed4(n, 0.0));
				float4 tpos2 = UnityObjectToClipPos(IN[1].pos + _Amount * pow(r2, 4) * fixed4(n, 0.0));
				float4 tpos3 = UnityObjectToClipPos(IN[2].pos + _Amount * pow(r3, 4) * fixed4(n, 0.0));

				float2 p0 = _ScreenParams.xy * tpos1.xy / tpos1.w;
				float2 p1 = _ScreenParams.xy * tpos2.xy / tpos2.w;
				float2 p2 = _ScreenParams.xy * tpos3.xy / tpos3.w;

				float2 edge0 = p2 - p1;
				float2 edge1 = p2 - p0;
				float2 edge2 = p1 - p0;

				float area = abs(edge1.x * edge2.y - edge1.y * edge2.x);

				g2f o;
				o.pos = tpos1;
				o.bary = float3(area/length(edge0), 0, 0);
				o.dist = pow(r1, _DistFade);
				triStream.Append(o);
				o.pos = tpos2;
				o.bary = float3(0, 0, area/length(edge1));
				o.dist = pow(r2, _DistFade);
				triStream.Append(o);
				o.pos = tpos3;
				o.bary = float3(0, area/length(edge2), 0);
				o.dist = pow(r3, _DistFade);
				triStream.Append(o);
			}

			g2f vert(g2f v) {
				g2f o;
				o.pos = v.pos;
				o.bary = v.bary;
				o.dist = v.dist;
				return o;
			}

			sampler2D _MainTex;
			float _WireframeVal;
			fixed4 _Color;
			fixed4 _FaceColor;
			float _Intensity;

			fixed4 frag(g2f i) : SV_Target 
			{
				float value = min(i.bary.x ,(min(i.bary.y,i.bary.z)));
				value = exp2(-1/_WireframeVal * value * value);
				float4 col = lerp(_Color * value * _Intensity, _FaceColor, 1.-value);
				col.a = clamp(1 / i.dist, 0, 1);
				
				return col;
			}

			ENDCG
		}


	}
}